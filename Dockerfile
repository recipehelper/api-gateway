FROM gradle:7.5-jdk17-alpine AS build
WORKDIR /opt/recipehelper/api-gateway
COPY ./ ./
RUN gradle bootJar --no-daemon --stacktrace

FROM eclipse-temurin:17-jre-alpine
WORKDIR /opt/recipehelper/api-gateway
COPY --from=build /opt/recipehelper/api-gateway/build/libs/api-gateway.jar ./
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "./api-gateway.jar"]
